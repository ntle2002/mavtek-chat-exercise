package com.example.chatdemo;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ChatRoomUnitTest {
    @Test
    public void testAdd_whenUserPresent_returnDifferentUser() {
        //setup
        ChatRoom room = new ChatRoom("toilet");
        room.addUser(new User("superman"));

        //action
        User user = room.addUser(new User("superman"));

        //assert
        assertNotEquals("superman", user.getNickname());
    }

    @Test
    public void testAdd_whenUserNotPresent_returnSameUser() {
        //setup
        ChatRoom room = new ChatRoom("toilet");

        //action
        User user = room.addUser(new User("superman"));

        //assert
        assertEquals("superman", user.getNickname());
    }
}

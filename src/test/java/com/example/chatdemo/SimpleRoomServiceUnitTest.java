package com.example.chatdemo;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class SimpleRoomServiceUnitTest {

    @Test
    public void testAdd_whenUserNotPresent_thenReturnSameUser() {
        //setup
        RoomService roomService = new SimpleRoomService();
        roomService.addRoom("toilet");
        //action
        User addedUser = roomService.addUser("toilet", new User("batman"));
        //assert
        assertEquals("batman", addedUser.getNickname());
    }

    @Test
    public void testAdd_whenUserPresent_thenReturnDifferentUser() {
        //setup
        RoomService roomService = new SimpleRoomService();
        roomService.addRoom("toilet");
        User firstUser = roomService.addUser("toilet", new User("batman"));
        //action
        User addedUser = roomService.addUser("toilet", new User("batman"));
        //assert
        assertNotEquals("batman", addedUser.getNickname());
    }
}
